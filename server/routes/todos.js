var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('mongodb://dkaramazov:realpassword@ds121543.mlab.com:21543/mean_todos', ['todos']);

// Get all todos
router.get('/todos', function(req, res, next){
    db.todos.find(function(err, todos){
        if(err){
            res.send(err);
        } else{
            res.json(todos);
        }
    });
});
// Get one todo
router.get('/todos/:id', function(req, res, next){
    db.todos.findOne({
        _id: mongojs.ObjectId(req.params.id)
    },function(err, todo){
        if(err){
            res.send(err);
        } else{
            res.json(todo);
        }
    });
});
// Save one todo
router.post('/todo', function(req, res, next){
    var todo = req.body;
    if(!todo.text || !(todo.isCompleted + '')){
        res.status(400);
        res.json({
            "error": "Invalid Data"
        });
    } else{
        db.todos.save(todo, function(err, result){
            if(err){
                res.send(err);
            } else{
                res.json(result);
            }
        });
    }
});
// Update one todo
router.put('/todo/:id', function(req, res, next){
    var todo = req.body;
    var updated = {};
    if(todo.isCompleted){
       updated.isCompleted = todo.isCompleted; 
    }
    if(todo.text){
        updated.text = todo.text;
    }
    if(!updated){
        res.status(400);
        res.json({
            "error":"Invalid Data"
        })
    } else{
        db.todos.update({
            _id: mongojs.ObjectId(req.params.id)
        },
        updated,
        {},
        function(err, result){
            if(err){
                res.send(err);
            } else{
                res.json(result);
            }
        });
    }
});
// Update one todo
router.delete('/todo/:id', function(req, res, next){
    db.todos.remove({
        _id: mongojs.ObjectId(req.params.id)
    }, 
    '',
    function(err, result){
        if(err){
            res.send(err);
        } else{
            res.json(result);
        }
    });
});
module.exports = router;