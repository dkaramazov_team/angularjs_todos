var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var cors = require('cors');

// load routes
var todos = require('./routes/todos');

var app = express();

app.use(express.static(path.join(__dirname, '../app')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// SERVE FILES FOR CLIENT DEPENDENCIES (ANGULAR, BOOTSTRAP, ETC)
app.use('/angular', express.static('node_modules/angular/angular.min.js'));
app.use('/uirouter', express.static('node_modules/@uirouter/angularjs/release/angular-ui-router.min.js'));
app.use('/bootstrap', express.static('node_modules/bootstrap/dist'));
app.use('/jquery', express.static('node_modules/jquery/dist/jquery.min.js'));
app.use('/', express.static('client'));

// setup API
app.use('/api/v1', todos);

var port = process.env.PORT || 8080;
app.listen(port, function() {
    console.log('Server started on port', port);
})