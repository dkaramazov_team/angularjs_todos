(function(){
    'use strict';

    angular
    .module('app')
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");        

        var homeState = {
            name: 'home',
            url: '/',
            templateUrl: 'views/home/home.html'
          }

        var aboutState = {
          name: 'about',
          url: '/about',
          templateUrl: 'views/about/about.html'
        }

        var giftState = {
          name: 'gift',
          url: '/gifts',
          templateUrl: 'views/gifts/gifts.html'
        }
      
        var todoState = {
            name: 'todo',
            url: '/todo',
            templateUrl: 'views/todo/todo.html'
          }

          $stateProvider.state(homeState);
          $stateProvider.state(aboutState);
          $stateProvider.state(giftState);
          $stateProvider.state(todoState);
      });

})()