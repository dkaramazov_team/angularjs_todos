(function () {
    'use strict';
    
    angular
        .module('app')
        .controller('todoCtrl', todoCtrl);

    todoCtrl.$inject = ['$scope', '$window', '$location', '$anchorScroll', 'appConfigSvc', 'todoSvc'];

    function todoCtrl($scope, $window, $location, $anchorScroll, appConfigSvc, todoSvc) {
        var vm = this;

        vm.todos = todoSvc.getAll().then(function (data) {
            vm.todos = data;
        });
        vm.todo_text = '';
        vm.add = add;
        vm.remove = remove;
        vm.updateStatus = updateStatus;
        vm.editing = editing;
        vm.update = update;

        function add(text){
            let result;
            let todo = {
              _id: '',
              text: text,
              isCompleted: false
            };

            function success(response){
                var obj = response.data;
                todo._id = obj._id;
                vm.todos.push(todo);
                vm.todo_text = '';
            }
            todoSvc.add(todo).then(success, error).catch();
        }

        function remove(id){
            function success(response){
                if(response.data){
                    for(var i = 0; i < vm.todos.length; i++){
                      if(vm.todos[i]._id === id){
                        vm.todos.splice(i, 1);
                        break;
                      }
                    }
                }
            }
            todoSvc.remove(id).then(success).catch(error);
        }

        function updateStatus(todo){
            let _todo = {
                _id: todo._id,
                text: todo.text,
                isCompleted: !todo.isCompleted
            }

            function success(response){
                todo.isCompleted = !todo.isCompleted;
            }
            todoSvc.update(_todo).then(success, error);
        }

        function update(todo){
            let _todo = {
                _id: todo._id,
                text: todo.text,
                isCompleted: false
            }

            function success(response){
                editing(todo, false);
                todo.text = todo.text;
            }
            todoSvc.update(_todo).then(success, error);
        }

        function editing(todo, state){
            if(state){
                todo.isEditing = state;
            } else{
                delete todo.isEditing;
            }
        }        
        
    };


    // Error handling
    function error(error){
        console.log(error);
    }

})();