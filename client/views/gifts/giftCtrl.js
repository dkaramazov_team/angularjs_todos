(function () {
    'use strict';
    
    angular
        .module('app')
        .controller('giftCtrl', giftCtrl);

    giftCtrl.$inject = ['$scope', '$window', '$location', '$anchorScroll', 'appConfigSvc', 'giftSvc'];

    function giftCtrl($scope, $window, $location, $anchorScroll, appConfigSvc, giftSvc) {
        var vm = this;
        vm.submitted = false;
        vm.levels = giftSvc.getLevels();
        vm.survey = giftSvc.getSurvey();
        vm.person = "";
        vm.response = {};

        vm.clearSurvey = function(survey){
            giftSvc.clearSurvey(survey);
        }
        
        vm.setAnswer = function(question, answer){
            giftSvc.setAnswer(question, answer);
        }

        vm.answerSurvey = function(survey){
            giftSvc.answerSurvey(survey);
        }

        vm.computeResponse = function(survey){
            vm.submitted = true;
            vm.response = giftSvc.computeResponse(survey);
            giftSvc.saveResponse(vm.person, vm.response);
        }

        vm.taken  = function(person){
            return localStorage.getItem(person) ? true:false;        
        }

        vm.viewResults = function(person){
            vm.response = JSON.parse(localStorage.getItem(person));
            vm.submitted = true;
        }
    };

})();