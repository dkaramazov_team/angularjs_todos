(function () {
    'use strict';

    angular
        .module('app')
        .factory('giftSvc', giftSvc);

    function giftSvc($http, $q, appConfigSvc) {
        var levels = [1, 2, 3, 4, 5];

        var survey = [
            {
                index: 1,
                text: "I quickly and accurately identify good and evil and I abhor evil.",
                answer: ""
            },
            {
                index: 2,
                text: "I do not want to lead others or lead projects.",
                answer: ""
            },
            {
                index: 3,
                text: "I feel Bible study is foundational to the operation of all the gifts.",
                answer: ""
            },
            {
                index: 4,
                text: "I view trials as opportunities for personal growth.",
                answer: ""
            },
            {
                index: 5,
                text: "I am definitely NOT gullible.",
                answer: ""
            },
            {
                index: 6,
                text: "I want to see things completed as quickly as possible.",
                answer: ""
            },
            {
                index: 7,
                text: "I am typically cheerful and joyful.",
                answer: ""
            },
            {
                index: 8,
                text: "I see everything as either black or white (no gray areas).",
                answer: ""
            },
            {
                index: 9,
                text: "I can easily recognize practical needs and I am quick to meet them.",
                answer: ""
            },
            {
                index: 10,
                text: "I love to study and do research.",
                answer: ""
            },
            {
                index: 11,
                text: "I love to encourage others to live victoriously.",
                answer: ""
            },
            {
                index: 12,
                text: "I love to give without the others knowing about it.",
                answer: ""
            },
            {
                index: 13,
                text: "I prefer to be under authority in order to have authority.",
                answer: ""
            },
            {
                index: 14,
                text: "I am always looking for the good in people rather then the bad.",
                answer: ""
            },
            {
                index: 15,
                text: "I encourage others to repentance that produces good fruit.",
                answer: ""
            },
            {
                index: 16,
                text: "I especially enjoy manual projects, jobs, and functions.",
                answer: ""
            },
            {
                index: 17,
                text: "I present truth in a logical systematic way.",
                answer: ""
            },
            {
                index: 18,
                text: "I want a visible response when I teach or speak.",
                answer: ""
            },
            {
                index: 19,
                text: "I intercede for needs and the salvation of souls.",
                answer: ""
            },
            {
                index: 20,
                text: "I will not take responsibility unless delegated by those in authority.",
                answer: ""
            },
            {
                index: 21,
                text: "I love opportunities to give preference or place to others.",
                answer: ""
            },
            {
                index: 22,
                text: "I love to do thoughtful things for others.",
                answer: ""
            },
            {
                index: 23,
                text: "I will endure criticism in order to accomplish the ultimate task.",
                answer: ""
            },
            {
                index: 24,
                text: "I want what I give to others to be of high quality or craftsmanship.",
                answer: ""
            },
            {
                index: 25,
                text: "I prefer to apply truth rather than research it.",
                answer: ""
            },
            {
                index: 26,
                text: "I emphasize facts and the accuracy of words.",
                answer: ""
            },
            {
                index: 27,
                text: "I enjoy showing hospitality.",
                answer: ""
            },
            {
                index: 28,
                text: "I have only a few or no close friends.",
                answer: ""
            },
            {
                index: 29,
                text: "I boldy operate on Biblical principles.",
                answer: ""
            },
            {
                index: 30,
                text: "I keep everyting in meticulous order.",
                answer: ""
            },
            {
                index: 31,
                text: "I enjoy word studies.",
                answer: ""
            },
            {
                index: 32,
                text: "I prefer systems of information that have practical application.",
                answer: ""
            },
            {
                index: 33,
                text: "I freely give money, possessions, time, energy, and love.",
                answer: ""
            },
            {
                index: 34,
                text: "I am highly motivated to organize that for which I am resposible.",
                answer: ""
            },
            {
                index: 35,
                text: "I have a tremendous capacity to show concern for others.",
                answer: ""
            },
            {
                index: 36,
                text: "I can sense the spiritual and emotional atmosphere of a group or individual.",
                answer: ""
            },
            {
                index: 37,
                text: "I will assume responsibilities if no specific leadership exists.",
                answer: ""
            },
            {
                index: 38,
                text: "I feel delighted when my gift to someone is an answer to a specific prayer they have prayed.",
                answer: ""
            },
            {
                index: 39,
                text: "I love to prescribe precise steps of action to aid personal growth.",
                answer: ""
            },
            {
                index: 40,
                text: "I prefer to use Biblical illustrations rather than life illustrations.",
                answer: ""
            },
            {
                index: 41,
                text: "I am a detail person with a good memory.",
                answer: ""
            },
            {
                index: 42,
                text: "I easily perceive the character of individuals and groups.",
                answer: ""
            },
            {
                index: 43,
                text: "I believe the acceptance of difficulties will produce positive personal brokenness.",
                answer: ""
            },
            {
                index: 44,
                text: "I will stay with a project or assignment until it is completed.",
                answer: ""
            },
            {
                index: 45,
                text: "I validate truth by checking out the facts.",
                answer: ""
            },
            {
                index: 46,
                text: "I focus on working with people.",
                answer: ""
            },
            {
                index: 47,
                text: "I want to feel a part of the ministry to which I contribute.",
                answer: ""
            },
            {
                index: 48,
                text: "I express ideas and organization in ways that communicate clearly.",
                answer: ""
            },
            {
                index: 49,
                text: "I am attracted to people who are hurting or in distress.",
                answer: ""
            },
            {
                index: 50,
                text: "I am ruled by my heart rather than by my head.",
                answer: ""
            },
            {
                index: 51,
                text: "I especially enjoy working with long-range goals and planning.",
                answer: ""
            },
            {
                index: 52,
                text: "I give only by the leading of the Holy Spirit.",
                answer: ""
            },
            {
                index: 53,
                text: "I encourage others to develop in their personal ministries.",
                answer: ""
            },
            {
                index: 54,
                text: "I get upset when Scripture is used out of context.",
                answer: ""
            },
            {
                index: 55,
                text: "I have a hard time saying \"no\" to requests for help.",
                answer: ""
            },
            {
                index: 56,
                text: "I view the Bible as the basis for truth, belief, action, and authority.",
                answer: ""
            },
            {
                index: 57,
                text: "I am frank, outspoken and don't mince words.",
                answer: ""
            },
            {
                index: 58,
                text: "I am more interested in meeting the needs of others than my own needs.",
                answer: ""
            },
            {
                index: 59,
                text: "I feel concerned that truth is established in every situation.",
                answer: ""
            },
            {
                index: 60,
                text: "I find truth in experience, then validate it with Scripture.",
                answer: ""
            },
            {
                index: 61,
                text: "I give to support and bless others or to advance a ministry.",
                answer: ""
            },
            {
                index: 62,
                text: "I am a visionary person with a broad perspective.",
                answer: ""
            },
            {
                index: 63,
                text: "I take action to remove hurts and relieve distress in others.",
                answer: ""
            },
            {
                index: 64,
                text: "I am more concerned about mental and emotional distress in others than I am with physical distress.",
                answer: ""
            },
            {
                index: 65,
                text: "I easily facilitate resources and people to accomplish tasks or goals.",
                answer: ""
            },
            {
                index: 66,
                text: "I view hospitality as an opportunity to give to others.",
                answer: ""
            },
            {
                index: 67,
                text: "I love to do personal counseling.",
                answer: ""
            },
            {
                index: 68,
                text: "I am more objective than subjective. In other words, I depend more on facts than feelings.",
                answer: ""
            },
            {
                index: 69,
                text: "I enjoy working on immediate goals rather than on longrange goals.",
                answer: ""
            },
            {
                index: 70,
                text: "I am very persuasive in public speaking.",
                answer: ""
            },
            {
                index: 71,
                text: "I show love for others in deeds and actions more than words.",
                answer: ""
            },
            {
                index: 72,
                text: "I have easily developed and I use a large vocabulary.",
                answer: ""
            },
            {
                index: 73,
                text: "I will discontinue personal counseling if the counselee shows no effort to change.",
                answer: ""
            },
            {
                index: 74,
                text: "I am able to handle finances with wisdom and frugality.",
                answer: ""
            },
            {
                index: 75,
                text: "I enjoy delegating tasks and supervising people.",
                answer: ""
            },
            {
                index: 76,
                text: "I am motiviated to help people have right relationships with one another.",
                answer: ""
            },
            {
                index: 77,
                text: "I take care with words and actions to avoid hurting others.",
                answer: ""
            },
            {
                index: 78,
                text: "I grieve deeply over the sins of others.",
                answer: ""
            },
            {
                index: 79,
                text: "I need to feel appreciated.",
                answer: ""
            },
            {
                index: 80,
                text: "I enjoy verifying what others teach.",
                answer: ""
            },
            {
                index: 81,
                text: "I am fluent in communication.",
                answer: ""
            },
            {
                index: 82,
                text: "I quickly volunteer to help where I see a need.",
                answer: ""
            },
            {
                index: 83,
                text: "I have great zeal and enthusiasm for whatever I am involved in.",
                answer: ""
            },
            {
                index: 84,
                text: "I easily detect insincerity or wrong motives.",
                answer: ""
            },
            {
                index: 85,
                text: "I find great fulfillment and joy in working to accomplish goals.",
                answer: ""
            },
            {
                index: 86,
                text: "I am eager to see my own blind spots and to help others see theirs too.",
                answer: ""
            },
            {
                index: 87,
                text: "I tend to do more than asked to do.",
                answer: ""
            },
            {
                index: 88,
                text: "I am trusting and trustworthy.",
                answer: ""
            },
            {
                index: 89,
                text: "I prefer teaching believers rather than engaging in evangelism.",
                answer: ""
            },
            {
                index: 90,
                text: "I accept people as they are without judging them.",
                answer: ""
            },
            {
                index: 91,
                text: "I seek confirmation on the amount to give.",
                answer: ""
            },
            {
                index: 92,
                text: "I am greatly loved because of my positive attitude.",
                answer: ""
            },
            {
                index: 93,
                text: "I have a strong belief in tithing and in giving in addition to tithing.",
                answer: ""
            },
            {
                index: 94,
                text: "I am willing to let others get the credit in order to get a job done.",
                answer: ""
            },
            {
                index: 95,
                text: "I solve problems by starting with Scriptural principles.",
                answer: ""
            },
            {
                index: 96,
                text: "I desire above all else to see God's plan worked out in all situations.",
                answer: ""
            },
            {
                index: 97,
                text: "I feel the greates joy in doing something that is helpful.",
                answer: ""
            },
            {
                index: 98,
                text: "I avoid conflicts and confrontatinos.",
                answer: ""
            },
            {
                index: 99,
                text: "I am intellectually sharp. (It's O.K. to answer honestly.)",
                answer: ""
            },
            {
                index: 100,
                text: "I focus on sharing the gospel.",
                answer: ""
            },
            {
                index: 101,
                text: "I strongly promote the spiritual growth of groups and individuals.",
                answer: ""
            },
            {
                index: 102,
                text: "I have a high energy level.",
                answer: ""
            },
            {
                index: 103,
                text: "I am drawn to people with the gift of compassion.",
                answer: ""
            },
            {
                index: 104,
                text: "I prefer to move on to a new challenge once something is completed.",
                answer: ""
            },
            {
                index: 105,
                text: "I prefer to witness with life rather than verbal witnessing.",
                answer: ""
            },
            {
                index: 106,
                text: "I constantly write notes to myself.",
                answer: ""
            },
            {
                index: 107,
                text: "I am called to intercession.",
                answer: ""
            },
            {
                index: 108,
                text: "I am self-disciplined.",
                answer: ""
            },
            {
                index: 109,
                text: "I make decisions easily.",
                answer: ""
            },
            {
                index: 110,
                text: "I don't like to be rushed in a job or activity.",
                answer: ""
            },
            {
                index: 111,
                text: "I cannot stand to be around clutter.",
                answer: ""
            },
            {
                index: 112,
                text: "I believe God is the source of His supply.",
                answer: ""
            },
            {
                index: 113,
                text: "I rejoice to see others blessed and grieve to see others hurt.",
                answer: ""
            },
            {
                index: 114,
                text: "I am a natural and capable leader.",
                answer: ""
            },
            {
                index: 115,
                text: "I feel the need to verbalize or dramatize what I see.",
                answer: ""
            },
            {
                index: 116,
                text: "I tend to be a perfectionist.",
                answer: ""
            },
            {
                index: 117,
                text: "I always complete what is started.",
                answer: ""
            },
            {
                index: 118,
                text: "I am emotionally self-controlled.",
                answer: ""
            },
            {
                index: 119,
                text: "I am very industrious with a tendency toward success.",
                answer: ""
            },
            {
                index: 120,
                text: "I have natural and effective business ability.",
                answer: ""
            },
            {
                index: 121,
                text: "I have only a selected circle of friends.",
                answer: ""
            },
            {
                index: 122,
                text: "I know when to keep old methods going and when to introduce new ones.",
                answer: ""
            },
            {
                index: 123,
                text: "I want to clear up problems with others quickly.",
                answer: ""
            },
            {
                index: 124,
                text: "I view serving to be of primary importance in life.",
                answer: ""
            },
            {
                index: 125,
                text: "I tend to be introspective (self-examining).",
                answer: ""
            },
            {
                index: 126,
                text: "I have strong opinions and convictions.",
                answer: ""
            },
            {
                index: 127,
                text: "I prefer doing a job to delegating it.",
                answer: ""
            },
            {
                index: 128,
                text: "I expect a lot of self and others.",
                answer: ""
            },
            {
                index: 129,
                text: "I am a crusader for good causes.",
                answer: ""
            },
            {
                index: 130,
                text: "I have strong convictions and opinions based on investigation of facts.",
                answer: ""
            },
            {
                index: 131,
                text: "I have strict personal standards.",
                answer: ""
            },
            {
                index: 132,
                text: "I like to get the best value for the money spent.",
                answer: ""
            },
            {
                index: 133,
                text: "I enjoy working with and being around people.",
                answer: ""
            },
            {
                index: 134,
                text: "I possess both natural and God-given wisdom.",
                answer: ""
            },
            {
                index: 135,
                text: "I desire to be obedient to God at all costs",
                answer: ""
            },
            {
                index: 136,
                text: "I do not enjoy doing routine tasks.",
                answer: ""
            },
            {
                index: 137,
                text: "I intercede for the hurts and problems of others.",
                answer: ""
            },
            {
                index: 138,
                text: "I believe truth has the intrinsic (inherent) power to produce change.",
                answer: ""
            },
            {
                index: 139,
                text: "I support others who are in leadership.",
                answer: ""
            },
            {
                index: 140,
                text: "I need a \"sounding board\" for bouncing off ideas and thoughts.",
                answer: ""
            }
        ];

        var service = {
            setAnswer: setAnswer,
            getLevels: getLevels,
            getSurvey: getSurvey,
            clearSurvey: clearSurvey,
            computeResponse: computeResponse,
            answerSurvey: answerSurvey,
            saveResponse: saveResponse
        };

        return service;

        function computeResponse(survey) {
            var dimensions = [
                'prophecy',
                'teaching',
                'exhortation',
                'wisdom',
                'knowledge',
                'leadership',
                'administration',
                'serving',
                'helps',
                'giving',
                'mercy',
                'hospitality',
                'faith',
                'discernment'
            ];
            var temp = {};
            var response = [];
            var index = 0;
            var count = 0;
            for (var i = 0; i < dimensions.length; i++) {
                temp[dimensions[i]] = 0;
                count = 0;
                index = i + 1;
                while (count < 9) {
                    temp[dimensions[i]] += Number(survey[index].answer);
                    index += 14;
                    count++;
                }
                response.push({ key: dimensions[i], value: temp[dimensions[i]] });
            }
            return response;
        }

        function setAnswer(question, answer) {
            question.answer = answer;
        }

        function getLevels() {
            return levels;
        }

        function getSurvey() {
            return survey;
        }

        function answerSurvey(survey) {
            for (var i = 0; i < survey.length; i++) {
                survey[i].answer = getRandomInt(1, 5);
                // survey[i].answer = 5;
            }
        }

        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        function clearSurvey(survey) {
            for (var i = 0; i < survey.length; i++) {
                survey[i].answer = "";
            }
        }

        function saveResponse(person, response) {
            localStorage.setItem(person, JSON.stringify(response));
        }

    };

})();
// compute survey results
// response.prophecy = survey[1] + survey[15] + survey[29] + survey[43] + survey[57] + survey[71] + survey[85] + survey[99] + survey[113]
// response.teaching = survey[2] + survey[16] + survey[30] + survey[44] + survey[58] + survey[72] + survey[86] + survey[100] + survey[114]
// response.exhortation = survey[3] + survey[17] + survey[31] + survey[45] + survey[59] + survey[73] + survey[87] + survey[101] + survey[115]
// response.wisdom = survey[4] + survey[18] + survey[32] + survey[46] + survey[60] + survey[74] + survey[88] + survey[102] + survey[116]
// response.knowledge = survey[5] + survey[19] + survey[33] + survey[47] + survey[61] + survey[75] + survey[89] + survey[103] + survey[117]
// response.leadership = survey[6] + survey[20] + survey[34] + survey[48] + survey[62] + survey[76] + survey[90] + survey[104] + survey[118]
// response.administration = survey[7] + survey[21] + survey[35] + survey[49] + survey[63] + survey[77] + survey[91] + survey[105] + survey[119]
// response.serving = survey[8] + survey[22] + survey[36] + survey[50] + survey[64] + survey[78] + survey[92] + survey[106] + survey[120]
// response.helps = survey[9] + survey[23] + survey[37] + survey[51] + survey[65] + survey[79] + survey[93] + survey[107] + survey[121]
// response.giving = survey[10] + survey[24] + survey[38] + survey[52] + survey[66] + survey[80] + survey[94] + survey[108] + survey[122]
// response.mercy = survey[11] + survey[25] + survey[39] + survey[53] + survey[67] + survey[81] + survey[95] + survey[109] + survey[123]
// response.hospitality = survey[12] + survey[26] + survey[40] + survey[54] + survey[68] + survey[82] + survey[96] + survey[110] + survey[124]
// response.faith = survey[13] + survey[27] + survey[41] + survey[55] + survey[69] + survey[83] + survey[97] + survey[111] + survey[125]
// response.discernment = survey[14] + survey[28] + survey[42] + survey[56] + survey[70] + survey[84] + survey[98] + survey[112] + survey[126]