(function () {
    'use strict';

    angular
        .module('app')
        .factory('todoSvc', todoSvc);

    function todoSvc($http, $q, appConfigSvc) {
        var service = {
            getAll: _getAll,
            add: add,
            remove: remove,
            update: update
        };

        return service;

        var headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token'
        }

        function add(todo){
            return $http.post(appConfigSvc.Session.ServiceUrl + '/todo', JSON.stringify(todo), {headers: headers});
        }

        function remove(id){
            return $http.delete(appConfigSvc.Session.ServiceUrl + '/todo/'+ id, {headers: headers});
        }

        function update(todo){
            return $http.put(appConfigSvc.Session.ServiceUrl + '/todo/' + todo._id, JSON.stringify(todo), {headers: headers} );
        }

        function _getAll() {
            function success(response) {
                return response.data;
            };
            function error(error) {
                console.log("Error: " + error);
            };
            return $http.get(appConfigSvc.Session.ServiceUrl + "/todos")
                .then(success, error)
                .catch(error);
        }
        
    };

})();