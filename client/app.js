(function () {
    'use strict';

    angular
        .module('app', ['ui.router'])
        .constant('appConfigSvc', (appConfigSvc)())
        .config(["$locationProvider", function($locationProvider){
            $locationProvider.html5Mode(true);
        }])

    function appConfigSvc() {
        return {
            Session: {
                Submitted: false,
                ServiceUrl: '/api/v1'
            }
        }
    }

})();